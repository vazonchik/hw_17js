$(document).ready(function(){
	$("#elem").click(function(){
		$("#elem").remove();
		$("body").append("<form id='form'></form>");
		$("#form").append("<label>Ведите радиус</label>");
		$("#form").append("<input id='rad'>");
		$("#form").append("<br>");
		$("#form").append("<label>Укажите цвет</label>");
		$("#form").append("<input id='col'>");
		$("#form").append("<br>");
		$("#form").append("<button id='button'>Нарисовать круг</button>");
		
		$("#button").click(function(){
			let rad = $("#rad").val();
			let col = $("#col").val();
			$("#form").remove();
			let round = $("body").append("<div id='round'></div>");
			$("#round").css({
				"background" : col,
				"height" : rad*2 + "px",
				"width"  : rad*2 + "px",
				"borderRadius" : "50%"
			})
		})
	})
})